Chonkers
========

# What

This project aims to leverage the user to view, add and edit Data-Chunks in PNG Files.

# Name

The name derives from the Chunks in PNG Files + rs since it was written in Rust. Swapping the u for an o was a given.


# Inspiration 

The inspiration for this software (and all of the tests, some of which I edited however) came from the great Picklenerd's [png_me book](https://picklenerd.github.io/pngme_book/introduction.html).
