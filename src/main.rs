#![allow(dead_code)]

mod chunk;
mod chunk_type;
mod png;
mod errs;
mod commands;

use clap::{App, load_yaml};
use std::path::Path;
use std::str::FromStr;

use crate::errs::StringError;
use crate::chunk_type::ChunkType;

pub type Error = Box<dyn std::error::Error>;
pub type Result<T> = std::result::Result<T, Error>;

fn main() -> Result<()> {
    let yaml = load_yaml!("../cli.yml");
    let matches = App::from(yaml).get_matches();

    // check for all that file is a file and chunk type is 4 letters
    match matches.subcommand() {
        Some((_, sub_match)) => {
            if let Some(filename) = sub_match.value_of("file") {
                if !Path::new(filename).is_file() {
                    return Err(Box::new(StringError::new(&format!("No such file {}", filename))))
                }
            }
            //if let Some(ctype_str) = sub_match.value_of("chunk_type") {
            //    if !(ctype_str.len() == 4 && ctype_str.is_ascii()){
            //        return Err(Box::new(StringError::new("Length of argument chunk type must be 4")))
            //    }
            //}
        }
        None => ()
    }

    // then act according to subcommand
    match matches.subcommand() {
        Some(("encode", sub_match)) => {
            let filename = sub_match.value_of("file").ok_or_else(|| Box::new(StringError::new("No value for required argument file")))?;
            let ctype = ChunkType::from_str(sub_match.value_of("chunk_type").ok_or_else(|| Box::new(StringError::new("No Value for required argument chunk_type.")))?)?;
            let message = sub_match.value_of("message").ok_or_else(|| Box::new(StringError::new("No value for required argument message")));
            commands::encode_to_file(filename, ctype, message, match sub_match.value_of("out_file") {
                Some(of) => of,
                None => filename
            })?;
        },
        Some(("decode", sub_match)) => {
            let filename = sub_match.value_of("file").ok_or_else(|| Box::new(StringError::new("No value for required argument file")))?;
            let ctype = ChunkType::from_str(sub_match.value_of("chunk_type").ok_or_else(|| Box::new(StringError::new("No Value for required argument chunk_type.")))?)?;
            // decode the given chunk type
            let message = commands::decode_from_file(filename, ctype.clone())?;
            println!("Message hidden in chunk '{}' is '{}'", ctype.to_string(), message);
        },
        Some(("remove", sub_match)) => {
            let filename = sub_match.value_of("file").ok_or_else(|| Box::new(StringError::new("No value for required argument file")))?;
            let ctype = ChunkType::from_str(sub_match.value_of("chunk_type").ok_or_else(|| Box::new(StringError::new("No Value for required argument chunk_type.")))?)?;
            // revome the chunk type
            match commands::remove_chunk_from_file(filename, ctype.clone()) {
                Ok(chunk_opt) => {
                    if let Some(ctype_str) = chunk_opt {
                        println!("Deleted chunk '{}'", ctype_str);
                    } else {
                        println!("No chunk with type '{}' in file '{}'\nTry subcommand 'print'", ctype.to_string(), filename);
                    }
                },
                Err(o) => println!("{}", o.to_string())
            }
        },
        Some(("print", sub_match)) => {
            let filename = sub_match.value_of("file").ok_or_else(|| Box::new(StringError::new("No value for required argument file")))?;
            // print all chunk types
            let info = commands::all_chunks(filename)?;
            println!("Chunks:\n-------");
            println!("{}", info);
        },
        _ => ()
    }
    Ok(())
}
