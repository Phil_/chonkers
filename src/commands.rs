use crate::chunk_type::ChunkType;
use crate::errs::StringError;
use crate::chunk::Chunk;
use crate::png::Png;
use crate::Result;

use std::string::String;
use std::fs;
use std::convert::TryFrom;

pub fn encode_to_file(infile: &str, ctype: ChunkType, message: &str, out_file: &str) -> Result<String> {
    // encodes the message in a new chunk with type ctype and saves the file as out_file with all
    // chunks from infile
    let mut png = png_from_file(infile)?;
    let new_chunk = Chunk::new(ctype, message.as_bytes());
    png.append_chunk(new_chunk);
    fs::write(out_file, png.as_bytes())?;
    Ok("Success".to_string())
}

pub fn decode_from_file(infile: &str, ctype: ChunkType) -> Result<String> {
    // attempts to decode the given chunk in the file
    let png = png_from_file(infile)?;
    match png.chunk_by_type(ctype) {
        Some(chunk) => Ok(chunk.data_as_string()?),
        None => Err(Box::new(StringError::new("No such chunk")))
    }
}

pub fn remove_chunk_from_file(infile: &str, ctype: ChunkType) -> Result<Option<String>> {
    // attempts to remove the given chunk from the file, returns the type name on success
    let mut png = png_from_file(infile)?;
    if let Some(chunk) = png.remove_chunk(ctype) {
        fs::write(infile, png.as_bytes())?;
        Ok(Some(chunk.ctype().to_string()))
    } else {
        Ok(None)
    }
}

pub fn all_chunks(infile: &str) -> Result<String> {
    // prints all chunks in the png file
    let png = png_from_file(infile)?;
    let foo: Vec<String> = png.chunks().iter()
        .map(|chunk| {format!("type: {}\nlen: {}",
            chunk.ctype().to_string(),
            chunk.length()
        )})
        .collect();
    Ok(format!("{}", foo.join("\n")).to_string())
}

fn png_from_file(infile: &str) -> Result<Png> {
    let data = fs::read(infile)?;
    Png::try_from(data.as_ref())
}

