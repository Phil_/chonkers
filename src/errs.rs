use std::fmt;
use std::error::Error;

#[derive(Debug)]
pub struct InvalidCRCError {
    given: u32,
    calculated: u32,
}

#[derive(Debug)]
pub struct StringError {
    details: String
}

//###################################################

impl InvalidCRCError {
    pub fn new(given: u32, calculated: u32) -> Self {
        Self { given, calculated }
    }
}

impl StringError {
    pub fn new(details: &str) -> Self {
        Self { details: details.to_string() }
    }
}

//###################################################

impl fmt::Display for InvalidCRCError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Expected: {} but calculated: {}", self.given, self.calculated)
    }
}

impl fmt::Display for StringError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.details)
    }
}

//###################################################

impl Error for InvalidCRCError {
    fn description(&self) -> &str {
        "crc sum doesnt match"
    }
}

impl Error for StringError {
    fn description(&self) -> &str {
        &self.details
    }
}

//###################################################
