extern crate crc;
use crc::crc32;

use std::fmt;
use std::string::String;
use std::convert::TryFrom;
use std::io::BufReader;
use std::io::prelude::*;

use crate::Result;
use crate::chunk_type::ChunkType;
use crate::errs::InvalidCRCError;



#[derive(Debug, Clone)]
pub struct Chunk {
    len: u32,
    ctype: ChunkType,
    message: Vec<u8>,
    crc: u32
}

impl TryFrom<&[u8]> for Chunk {
    type Error = Box<dyn std::error::Error>;

    fn try_from(value: &[u8]) -> Result<Self> {
        let mut r = BufReader::new(value);
        let mut buffer: [u8; 4] = [0; 4];

        // lenght of content
        r.read_exact(&mut buffer)?;
        let len = u32::from_be_bytes(buffer);

        // buffer type
        r.read_exact(&mut buffer)?;
        let ctype = ChunkType::try_from(buffer)?;

        // content
        let mut message = Vec::with_capacity(len as usize);
        message.resize(len as usize, 0);
        r.read_exact(&mut message)?;

        // crc sum in data
        r.read_exact(&mut buffer)?;
        let crc_given = u32::from_be_bytes(buffer);

        // self calculated crc sum
        let crc = crc32::checksum_ieee(&value[4..(8+len) as usize]);

        if crc == crc_given {
            Ok(Chunk {len, ctype, message, crc})
        } else {
            Err(Box::new(InvalidCRCError::new(crc_given, crc)))
        }
    }
}

impl fmt::Display for Chunk {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self.as_bytes())
    }
}

impl Chunk {
    pub fn new(ctype: ChunkType, message: &[u8]) -> Chunk {
        let len = message.len() as u32;
        let message = message.to_vec();
        let mut all: Vec<u8> = ctype.bytes().iter().map(|x| *x).collect();
        all.append(&mut message.clone());
        let crc = crc32::checksum_ieee(&all);
        Chunk { len, ctype, message, crc }
    }

    pub fn new_with_crc(ctype: ChunkType, message: &[u8], crc: u32) -> Result<Chunk> {
        let bytes: Vec<u8> = (message.len() as u32)
            .to_be_bytes()
            .iter()
            .chain(ctype.bytes().iter())
            .chain(message.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();
        Chunk::try_from(bytes.as_ref())

    }

    pub fn length(&self) -> u32 {
        self.message.len() as u32
    }

    pub fn ctype(&self) -> &ChunkType {
        &self.ctype
    }

    pub fn data(&self) -> &[u8] {
        &self.message
    }

    pub fn crc(&self) -> u32 {
        self.crc
    }

    pub fn data_as_string(&self) -> Result<String> {
        match std::str::from_utf8(self.message.as_ref()) {
            Ok(data) => Ok(data.to_string()),
            Err(o) => Err(Box::new(o))
        }
    }

    pub fn as_bytes(&self) -> Vec<u8> {
        self.length()
            .to_be_bytes()
            .iter()
            .chain(self.ctype.to_string().as_bytes().iter())
            .chain(self.message.iter())
            .chain(self.crc.to_be_bytes().iter())
            .copied()
            .collect()
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    fn testing_chunk() -> Chunk {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        Chunk::try_from(chunk_data.as_ref()).unwrap()
    }

    #[test]
    fn test_chunk_length() {
        let chunk = testing_chunk();
        assert_eq!(chunk.length(), 42);
    }

    #[test]
    fn test_chunk_type() {
        let chunk = testing_chunk();
        assert_eq!(chunk.ctype().to_string(), String::from("RuSt"));
    }

    #[test]
    fn test_chunk_string() {
        let chunk = testing_chunk();
        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");
        assert_eq!(chunk_string, expected_chunk_string);
    }

    #[test]
    fn test_chunk_crc() {
        let chunk = testing_chunk();
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_valid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref()).unwrap();

        let chunk_string = chunk.data_as_string().unwrap();
        let expected_chunk_string = String::from("This is where your secret message will be!");

        assert_eq!(chunk.length(), 42);
        assert_eq!(chunk.ctype().to_string(), String::from("RuSt"));
        assert_eq!(chunk_string, expected_chunk_string);
        assert_eq!(chunk.crc(), 2882656334);
    }

    #[test]
    fn test_invalid_chunk_from_bytes() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656333;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk = Chunk::try_from(chunk_data.as_ref());

        assert!(chunk.is_err());
    }

    #[test]
    pub fn test_chunk_trait_impls() {
        let data_length: u32 = 42;
        let chunk_type = "RuSt".as_bytes();
        let message_bytes = "This is where your secret message will be!".as_bytes();
        let crc: u32 = 2882656334;

        let chunk_data: Vec<u8> = data_length
            .to_be_bytes()
            .iter()
            .chain(chunk_type.iter())
            .chain(message_bytes.iter())
            .chain(crc.to_be_bytes().iter())
            .copied()
            .collect();

        let chunk: Chunk = TryFrom::try_from(chunk_data.as_ref()).unwrap();

        let _chunk_string = format!("{}", chunk);
    }
}
